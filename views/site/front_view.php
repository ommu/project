<?php
/**
 * @var $this SiteController
 * @var $model Projects
 *
 * @author Putra Sudaryanto <putra@ommu.co>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2014 Ommu Platform (www.ommu.co)
 * @link https://bitbucket.org/ommu/project
 *
 */
 
	$this->breadcrumbs=array(
		'Projects'=>array('manage'),
		$model->title,
	);
?>

<?php //begin.Messages ?>
<?php
if(Yii::app()->user->hasFlash('success'))
	echo $this->flashMessage(Yii::app()->user->getFlash('success'), 'success');
?>
<?php //end.Messages ?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'project_id',
		'publish',
		'cat_id',
		'user_id',
		'media_id',
		'headline',
		'comment_code',
		'title',
		'body',
		'status',
		'start_date',
		'finish_date',
		'comment',
		'view',
		'likes',
		'creation_date',
		'modified_date',
	),
)); ?>
